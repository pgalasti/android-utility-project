package com.gwiz.androidutils.logging;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.widget.Toast;

/**
 * Base class used for logging events to internal application space. All messages 
 * logged will be appended at the end of the private file allowing a trace of 
 * messages.
 * <p>
 * Since the class is a low level base class, any internal errors cannot be caught, 
 * and will only generate a {@link android.widget.Toast Toast} message of a failure. 
 * This class should only be used as a last case result of logging an event if another 
 * option is not available.
 * <p>
 * 
 * @author pgalasti@gmail.com
 */
public class Logger {

	private Context context;
	private String fileName;
	private FileOutputStream fos;
	
	/**
	 * Base logging class for logging specific events. The logging file is private to the  
	 * application storage space, and is only available to the application itself.
	 * <p>
	 * All messages are appended to the end of the file concatenated with a newline character '\n'. 
	 * If the log file does not exist at time of writing, the file will be created.
	 * <p>
	 * 
	 * @param context The application context.
	 * @param fileName The name of the log file to be used. 
	 */
	public Logger(final Context context, final String fileName)
	{
		this.context = context;
		setFileName(fileName);
	}
	
	/**
	 * Sets the current log file name that is being written to.
	 * @param fileName The new log file name.
	 */
	public void setFileName(final String fileName)
	{
		this.fileName = fileName;
	}
	
	/**
	 * Writes the string message to the end of the logging file. Each string message is 
	 * concatenated with a newline character <b>'\n'</b>. If the file does not exist 
	 * while being written to, the file will be created.
	 * <p>
	 * Any errors writing to the file will generate a {@link android.widget.Toast Toast} 
	 * message detailing the error.
	 * 
	 * @param message The message to be written to the end of the file.
	 */
	public void writeToFile(final String message)
	{
		try {
			openFile();
		} catch (FileNotFoundException e) {
			Toast.makeText(context, "resource goes here", Toast.LENGTH_LONG).show();
			return;
		}
		
		try {
			fos.write(message.concat("\n").getBytes());
		} catch (IOException e) {
			Toast.makeText(context, "resource goes here", Toast.LENGTH_LONG).show();
			return;
		}
		
		try {
			fos.close();
		} catch (IOException e) {
			Toast.makeText(context, "resource goes here", Toast.LENGTH_LONG).show();
			return;
		}
	}
	
	/**
	 * Deletes the current log file if it exists.
	 */
	public void deleteLogFile()
	{
		context.deleteFile(fileName);
		fos = null;
	}
	
	
	private void openFile() throws FileNotFoundException
	{
		fos = null;
		fos = context.openFileOutput(fileName, Context.MODE_APPEND | Context.MODE_PRIVATE);
	}	
}
