package com.gwiz.androidutils.db;


import android.database.Cursor;

/**
 * Basic database record layer between application logic and database. Each child 
 * of AQueryRecord should represent a specific table of the database.
 * <p>
 * 
 * AQueryRecords contain methods traversing the cursor state of a table queried  
 * along the ability to update corresponding database records with data loaded 
 * in secondary fields.
 * <p>
 *
 *  AQueryRecords follow the {@link com.gwiz.androidutils.db.IQueryWritable IQueryWritable} 
 *  and {@link com.gwiz.androidutils.db.IQueryReadable IQueryReadable} implementing 
 *  creation for specific SQL strings and executing SQL commands within the AQueryRecord.
 *  <p>
 *    
 * @author pgalasti@gmail.com
 */

public abstract class AQueryRecord implements IQueryWritable, IQueryReadable{

	protected Cursor m_Cursor;
	
	/**
	 * Checks if the cursor is in a valid state.
	 * @return 	<b>True</b> if the cursor is in valid state.
	 * 			<br><b>False</b> if cursor is invalid.
	 */
	public boolean getGoodState(){
		if( m_Cursor.getCount() > 0 )
			return true;
		return false;
	}
	
	/**
	 * First result of query performed.
	 * @throws DBException
	 */
	public void getFirst() throws DBException
	{
		if( m_Cursor.getCount() == 0 )
			return;
		
		if( !m_Cursor.moveToFirst() && m_Cursor.getCount() != 0)
			throw new DBException("Cursor state unable to move to first record!");
		
		BindData();
	}
	
	/**
	 * Last result of query performed.
	 * @throws DBException
	 */
	public void getLast() throws DBException
	{
		if( !getGoodState() )
			throw new DBException("Cursor state is not valid!");
		if( !m_Cursor.moveToLast() && m_Cursor.getCount() > 0 )
			throw new DBException("Cursor state unable to move to last record!");
		
		BindData();
	}
	
	/**
	 * Moves cursor to next row of query.
	 * @return 	<b>True</b> if cursor moved to next row.
	 * 			</br><b>False</b> if there are no more rows.
	 * @throws DBException
	 */
	public boolean readNext() throws DBException
	{
		if( !getGoodState() )
			throw new DBException("Cursor state is not valid!");
		
		if( !m_Cursor.move(1) )
			return false;

		if( m_Cursor.getPosition() == m_Cursor.getCount())	// Hackish. Cursor doesn't know if limit is reached.
			return false;
		
		BindData();
		return true;
	}
	
	/**
	 * Binds current cursor result to object instance record data. Child 
	 * classes should override with how their records bind with database 
	 * fields.
	 */
	protected abstract void BindData();
	
	/**
	 * Returns an implemented {@link com.gwiz.androidutils.db.IRecordAttributes IDBRecord} 
	 * containing all important database table information.
	 * <p>
	 * 
	 * @return The AQueryRecord specific 
	 * {@link com.gwiz.androidutils.db.IRecordAttributes IDBRecord}.
	 */
	public abstract IRecordAttributes getRecordAttributes();
	
	/**
	 * Returns a string detailing the current record field data. This should only be
	 * for debug information.
	 * <p>
	 * 
	 * String should be specific to table layout, but should follow convention 
	 * of simple field to value detail.
	 * <p>
	 * 
	 * Use convention of: </br>
	 * <b>[field1]:[type]:[value]</b>[NEWLINE]</br>
	 * <b>[field2]:[type]:[value]</b>[NEWLINE]</br>
	 * <b>etc...</b></br>
	 * <p>
	 * 
	 * @return String detailing current record field data.
	 */
	public abstract String getDebugDetail();
	
	
}
