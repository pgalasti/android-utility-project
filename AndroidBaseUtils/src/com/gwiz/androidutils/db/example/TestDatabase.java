package com.gwiz.androidutils.db.example;

import android.content.Context;

import com.gwiz.androidutils.db.Database;

/**
 * This is an example record of how to extend {@link com.gwiz.androidutils.db.Database Database} 
 * @author pgalasti@gmail.com
 *
 */
public class TestDatabase extends Database {

	public TestDatabase(Context context, int version) {
		super(context, "testdatabase.db", version);
	}

	@Override
	protected void setViewList() {
		TestRecord testRecord = new TestRecord();
		// Additional records would be added here for the database.
		
		viewList.add(testRecord.getRecordAttributes());
	}

	@Override
	public String getDatabaseName() {
		return "testdatabase.db";
	}

	@Override
	public String getDBMSName() {
		return "Paul's Magical DBMS";
	}

	@Override
	public String getDBMSVersion() {
		return "1.0";
	}

}
