package com.gwiz.androidutils.db.example;

import android.content.ContentValues;

import com.gwiz.androidutils.db.AQueryRecord;
import com.gwiz.androidutils.db.DBException;
import com.gwiz.androidutils.db.Database;
import com.gwiz.androidutils.db.IRecordAttributes;

/**
 * This is an example record of how to extend {@link com.gwiz.androidutils.db.AQueryRecord AQueryRecord} 
 * and implement database query operations.
 * <p>
 * 
 * @author pgalasti@gmail.com
 */
public class TestRecord extends AQueryRecord {

	String strField = "";
	int intField = 0;
	long dateField = 0;
	
	
	@Override
	public int update(Database db) throws DBException {
		ContentValues cv = new ContentValues();
		cv.put("intfield", intField);
		cv.put("datefield", dateField);
		
		return db.getWritableDatabase().update(getRecordAttributes().getTableName(), cv, "strfield=?", new String[]{strField});
	}

	@Override
	public long insert(Database db) throws DBException {
		ContentValues cv = new ContentValues();
		cv.put("strfield", strField);
		cv.put("intfield", intField);
		cv.put("datefield", dateField);
		
		long insertID = db.getWritableDatabase().insert(getRecordAttributes().getTableName(), null, cv);
		if( insertID == -1 )
		{
			StringBuilder sbError = new StringBuilder();
			sbError.append("strfield = ").append(strField)
			.append("intfield = ").append(intField)
			.append("datefield = ").append(dateField);
			
			throw new DBException("Insertion", this);
		}
		
		return insertID;
	}

	@Override
	public int delete(Database db) throws DBException {
		return db.getWritableDatabase().delete(getRecordAttributes().getTableName(), "strfield=?", new String[]{strField});
	}

	@Override
	public void select(Database db, String sqlQuery) throws DBException {
		m_Cursor = db.getWritableDatabase().rawQuery(sqlQuery, null);
		this.getFirst();
	}

	@Override
	public void select(Database db, int keys) throws DBException {
		if( keys < 1 )
		{
			m_Cursor = db.getWritableDatabase().query(getRecordAttributes().getTableName(), null, null, null, null, null, null);
			this.getFirst();
			return;
		}
		
		StringBuilder sbSelection = new StringBuilder("strfield=?");
		String[] selecetionArgs = new String[keys];
		selecetionArgs[0] = strField;
		if( keys > 1 )
		{
			sbSelection.append(" AND intfield=?");
			selecetionArgs[1] = String.valueOf(intField);
		}
		if( keys > 2 )
		{
			sbSelection.append(" AND datefield=?");
			selecetionArgs[2] = String.valueOf(dateField);
		}
		
		m_Cursor = db.getWritableDatabase().query(getRecordAttributes().getTableName(), null, sbSelection.toString(), selecetionArgs, null, null, null);
		this.getFirst();
	}

	@Override
	protected void BindData() {
		short index = 0;
		strField = m_Cursor.getString(index++);
		intField = m_Cursor.getInt(index++);
		dateField = m_Cursor.getLong(index++);
	}

	
	
	protected static IRecordAttributes attributes;
	static 	{
		attributes =  new IRecordAttributes() {

			@Override
			public String getTableName() {
				return "testtable";
			}

			@Override
			public int getTableColumns() {
				return 3;
			}

			@Override
			public String getSQLCreateStatement() {
				StringBuilder SB = new StringBuilder();
				SB.append("CREATE TABLE ").append(getTableName()).append("(")
				.append("strfield TEXT PRIMARY KEY, ")
				.append("intfield INTEGER, ")
				.append("datefield LONG);");

				return SB.toString();
			}

			@Override
			public String getSQLAlterSchema(int oldVersion, int newVersion) {
				return "";
			}
		};
	}
	
	@Override
	public IRecordAttributes getRecordAttributes() {
		return attributes;
	}

	@Override
	public String getDebugDetail() {
		StringBuilder SB = new StringBuilder();
		SB.append("Table:").append(attributes.getTableName()).append("\n")
		.append("Columns:").append(attributes.getTableColumns()).append("\n")
		.append("strField:").append("TEXT:").append(strField).append("\n")
		.append("intField:").append("INTEGER:").append(intField).append("\n")
		.append("dateField:").append("LONG:").append(dateField).append("\n");
		return SB.toString();
	}

}
