package com.gwiz.androidutils.db;

import android.database.sqlite.SQLiteException;

public class DBException extends SQLiteException {

	private static final long serialVersionUID = -8716221072840674483L;
	
	public DBException(String errorMessage)
	{
		super(errorMessage);
	}
	
	public DBException (String errorMessage, Database db)
	{
		super(errorMessage);
	}
	
	public DBException (String errorMessage, AQueryRecord record)
	{
		super(errorMessage + record.getDebugDetail());
	}

}
