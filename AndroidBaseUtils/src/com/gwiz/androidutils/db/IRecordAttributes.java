package com.gwiz.androidutils.db;

/**
 * An interface for describing specific table information.
 * <p>
 * 
 * @author pgalasti@gmail.com
 */
public interface IRecordAttributes {

	/**
	 * Returns the database table name.
	 * @return The database table name.
	 */
	public String 	getTableName();
	
	/**
	 * Returns the columns of the table.
	 * @return The columns of the table.
	 */
	public int		getTableColumns();
	
	/**
	 * The SQL creation statement for the table.
	 * @return The SQL creation statement for the table.
	 */
	public String	getSQLCreateStatement();
	
	/**
	 * The SQL alteration statement for the table between versions. Each version jump 
	 * should be defined here and how it is handled.
	 * @param oldVersion The old version number the current database table is coming from.
	 * @param newVersion The new version number the database table supports.
	 * @return The SQL alteration statement for the table between versions.
	 */
	public String 	getSQLAlterSchema(int oldVersion, int newVersion);
}
