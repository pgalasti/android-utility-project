package com.gwiz.androidutils.ui.stickylistheaders;

import android.content.Context;
import android.widget.SectionIndexer;

//All credit to Emil Sj�lander sjolander.emil@gmail.com https://github.com/emilsjolander/StickyListHeaders


class SectionIndexerAdapterWrapper extends
		AdapterWrapper implements SectionIndexer {
	
	final SectionIndexer mSectionIndexerDelegate;

	SectionIndexerAdapterWrapper(Context context,
			StickyListHeadersAdapter delegate) {
		super(context, delegate);
		mSectionIndexerDelegate = (SectionIndexer) delegate;
	}

	@Override
	public int getPositionForSection(int section) {
		return mSectionIndexerDelegate.getPositionForSection(section);
	}

	@Override
	public int getSectionForPosition(int position) {
		return mSectionIndexerDelegate.getSectionForPosition(position);
	}

	@Override
	public Object[] getSections() {
		return mSectionIndexerDelegate.getSections();
	}

}
